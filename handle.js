
people = [['Seymour','BOS'],
['Franny','DAL'],
['Zooey','CAK'],
['Walt','MIA'],
['Buddy','ORD'],
['Les','OMA']];

salary = [53, 86, 76, 67, 85, 42];
function convertSalary(salary){
    convert = [];
    for (var i=0; i<salary.length;i++){
        convert[i] = salary[i]/60;
    }
    return convert;
}
costPerMin = convertSalary(salary);
console.log("Cost of waiting each person:");
console.log(costPerMin);

var destination='LGA';
var flight = [];
function getminutes(t){ //convert time from hour to minute
    var position = t.lastIndexOf(':');
    return Number(t.slice(0,position))*60 + Number(t.slice(position+1,));
}

function readFile(lines){ //read file text and put it into flight variable
    for (var line = 0; line < lines.length; line++){
        info = lines[line].split(',');
        if (line == 0)
            flight.push({'schedule': {'origin': info[0], 'dest': info[1]}, 'time': [{'depart': info[2], 'arrive': info[3], 'price': info[4]}]});
        else {
            var i=0;
            for (i; i<flight.length; i++){
                if (flight[i].schedule.origin == info[0] && flight[i].schedule.dest == info[1])
                {
                    flight[i].time.push({'depart': info[2], 'arrive': info[3], 'price': info[4]});
                    break;
                }
            }
            if (i == flight.length)
                flight.push({'schedule': {'origin': info[0], 'dest': info[1]}, 'time': [{'depart': info[2], 'arrive': info[3], 'price': info[4]}]});
        }

    }
}

function printFlight(){ //print data of flight
    var a = '';
    for (var i = 0; i<flight.length; i++){
        a += '(' + flight[i].schedule.origin + ',' + flight[i].schedule.dest + '): [';
        for (var j = 0; j<flight[i].time.length; j++){
            a += '(' + flight[i].time[j].depart + ',' + flight[i].time[j].arrive + ',' + flight[i].time[j].price + '),';
        }
        a += '],\n';
    }
    //document.getElementById("data").innerHTML = a + "\nlength flight:" + flight.length;
}

function printschedule(r){ //print a particular schedule
    info = '';
    for (var i=0; i<r.length/2; i++){
        name=people[i][0];
        origin=people[i][1];
        var out=[], ret=[];
        for (var j = 0; j<flight.length; j++){
            if (flight[j].schedule.origin == destination && flight[j].schedule.dest == origin){
                ret.push(flight[j].time[r[i*2+1]].depart,flight[j].time[r[i*2+1]].arrive, flight[j].time[r[i*2+1]].price);
            }
            if (flight[j].schedule.origin == origin && flight[j].schedule.dest == destination){
                out.push(flight[j].time[r[i*2]].depart,flight[j].time[r[i*2]].arrive, flight[j].time[r[i*2]].price);
                break;
            }
            
        }
        info += name + ' \t\t' + origin + ' \t\t' + out[0] + '-' + out[1] + '  $' + out[2] + ' \t\t' + ret[0] + '-' + ret[1] + '  $' + ret[2] + '\t\n';
        //document.getElementById("print").innerHTML = info;
    }
    console.log(info);
}


function schedulecost(sol){ //evaluate cost of a particular schedule
    var totalprice=0, latestarrival=0, earliestdep=24*60;
    for (var i=0; i<sol.length/2; i++){
        //Get the inbound and outbound flights
        var origin=people[i][1];
        var outbound = ['0:0','0:0','0'], returnf = ['0:0','0:0','0'];
        for (var j = 0; j<flight.length; j++){
            if (flight[j].schedule.origin == origin && flight[j].schedule.dest == destination){
                outbound = [flight[j].time[sol[i*2]].depart,flight[j].time[sol[i*2]].arrive, flight[j].time[sol[i*2]].price];
                returnf = [flight[j-1].time[sol[i*2+1]].depart,flight[j-1].time[sol[i*2+1]].arrive, flight[j-1].time[sol[i*2+1]].price];
                //Total price is the price of all outbound and return flights
                totalprice+=Number(outbound[2]) + Number(returnf[2]);
                //Track the latest arrival and earliest departure
                //console.log(i + '  getminute outbound: ' + getminutes(outbound[1]) + '  getminute returnf: ' + getminutes(returnf[0]) + "   returnf: " + returnf[0]);
                if (latestarrival < getminutes(outbound[1]))
                    latestarrival = getminutes(outbound[1]);
                if (earliestdep > getminutes(returnf[0]))
                    earliestdep = getminutes(returnf[0]);
            }
        }
    }
    //Every person must wait at the airport until the latest person arrives.
    //They also must arrive at the same time and wait for their flights.
    var totalwait=0;
    //console.log("latestarrival: " + latestarrival + "\tearliestdep: " + earliestdep)
    for (var i = 0; i<sol.length/2; i++){
        origin=people[i][1];
        var outbound = ['0:0','0:0','0'], returnf = ['0:0','0:0','0'];
        for (var j = 0; j<flight.length; j++){
            if (flight[j].schedule.origin == origin && flight[j].schedule.dest == destination){
                outbound = [flight[j].time[sol[i*2]].depart,flight[j].time[sol[i*2]].arrive, flight[j].time[sol[i*2]].price];
                returnf = [flight[j-1].time[sol[i*2+1]].depart,flight[j-1].time[sol[i*2+1]].arrive, flight[j-1].time[sol[i*2+1]].price];
                totalwait+=(latestarrival-getminutes(outbound[1]))*costPerMin[i];
                totalwait+=(getminutes(returnf[0])-earliestdep)*costPerMin[i]; 
            }
        }
    }
    //Does this solution require an extra day of car rental? That'll be $50!
    if (latestarrival>earliestdep) totalprice+=50;
    return totalprice+totalwait;
}

function ranNum(){ //random Number between 0 and 9
    return Math.floor((Math.random() * 10));
}

function condition(max, cost){
    if (max > cost){
        return 1;
    }
    else return 0;
}

function random(){ //random n times in flight variable to take min's cost of schedule
    var max = 5000;
    var min = null;
    var str;
    var loop = 100000;
    for (var i=0; i<loop; i++){
        s = [ranNum(),ranNum(),ranNum(),ranNum(),ranNum(),ranNum(),ranNum(),ranNum(),ranNum(),ranNum(),ranNum(),ranNum()];
        //console.log(s);
        var cost = schedulecost(s);
        /*if (condition(max, cost) == 1)
            return [cost, loop, s];
    }*/
         if (i == 0){
             min = cost;
             str = s;
         }
         else if(min > cost){
             min = cost;
             str = s;
         }
     }
     return [min, loop, str];
}

function loopRandom(){ // loop n times of function random
    min = null;
    for (var i = 0; i < 10; i++){
        cost = random();
        if (i == 0)
            min = cost;
        else if(min[0] > cost[0])
            min[0] = cost[0];
    }
    return min;
}

function vetcan(){ //exhausted of flight schedule to take the min's cost of all schedule
    str = [0,0,0,0,0,0,0,0,0,0,0,0];
    min = schedulecost(str);
    for (var a=0; a<10; a++){
        for (var b=0; b<10; b++){
            for (var c=0; c<10; c++){
                for (var d=0; d<10; d++){
                    for (var e=0; e<10; e++){
                        for (var f=0; f<10; f++){
                            for (var g=0; g<10; g++){
                                for (var h=0; h<10; h++){
                                    for (var i=0; i<10; i++){
                                        for (var j=0; j<10; j++){
                                            for (var k=0; k<10; k++){
                                                for (var l=0; l<10; l++){
                                                    s = [a,b,c,d,e,f,g,h,i,j,k,l];
                                                    cost = schedulecost(s);
                                                    if(min > cost){
                                                        min = cost;
                                                        str = s;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return [min, str];
}

function hillclimb(){
    //Create a random solution
    sol = [ranNum(),ranNum(),ranNum(),ranNum(),ranNum(),ranNum(),ranNum(),ranNum(),ranNum(),ranNum(),ranNum(),ranNum()];
    //Main loop
    while (1) {
        //Create list of neighboring solutions
        var neighbors=[];
        for (var j = 0; j < sol.length; j++){
            arr = new Array();
            arr1 = new Array();
            //One away in each direction
            if (sol[j]>0){
                for (i=0;i<j;i++)
                    arr.push(sol[i]);
                arr.push(sol[j] - 1);
                for (i=j+1;i<sol.length;i++)
                    arr.push(sol[i]);
                neighbors.push(arr);
            }
            if (sol[j]<9){
                for (i=0;i<j;i++)
                    arr1.push(sol[i]);
                arr1.push(sol[j] + 1);
                for (i=j+1;i<sol.length;i++)
                    arr1.push(sol[i]);
                neighbors.push(arr1);
            }
        }
        //See what the best solution amongst the neighbors is
        current = schedulecost(sol);
        best = current;
        for (i=0;i<neighbors.length;i++){
            cost = schedulecost(neighbors[i]);
            if (cost < best){
                best = cost;
                sol = neighbors[i];
            }
        }
        //If there's no improvement, then we've reached the top
        if (best == current)
            break;
    }
    return [sol, best];
}

jQuery.get('http://localhost/travel/schedule.txt', function(data) {
    var lines = data.split('\n');
    readFile(lines);
    printFlight();
    //console.log('price "' + flight[3].time[2].depart + '"');
    // s=[1,4,3,2,7,3,6,3,2,4,5,3];
    // printschedule(s);
    // console.log('cost: ' + schedulecost(s) + '\t[' + s + ']');
    var x = random();
    console.log("Random:");
    console.log(x);
	document.write(" Random:  <br> Cost: ");
	document.write(x[0]);
	document.write("<br>");
	document.write("So Lan Random: ");
	document.write(x[1]);
	document.write("<br>");
	document.write("S [ ");
	document.write(x[2]);
	document.write("]");
    console.log("Hillclimb:");
    console.log(b = hillclimb());
	document.write(' <br> Hill Climb: <br> S[ ');
	document.write(b[0] + " ] <br> cost: " + b[1] );
	
	
});