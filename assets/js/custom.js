$('.owl-carousel').owlCarousel({
    loop:false,
    margin:30,
    nav: true,
    dots: false,
    responsive:{
    	0:{
    		items:1
    	},
        1000:{
            items:3
        }
    }
})